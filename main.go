package main

import (
	"flag"
	"fmt"
)

func main() {
	// declare flag
	name := flag.String("name", "you", "your name")

	// parse
	flag.Parse()

	// do treatment
	action := flag.Arg(0)
	sentences := fmt.Sprintf("Hello %s, let's go to %s", *name, action)
	fmt.Println(sentences)

	fmt.Println("---")

	// print help
	flag.PrintDefaults()

	fmt.Println("---")

	// print all args
	for _, arg := range flag.Args() {
		fmt.Print(arg, " ")
	}

}
